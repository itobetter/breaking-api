---
theme: seriph
class: text-center
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)

transition: slide-left
title: Welcome to Slidev
---

<img src="/imgs/intro.jpeg">

---
transition: fade-out
layout: center
---

<div grid="~ cols-2">
<div class="self-center justify-self-center">

## Definición de API​

</div>
<div>

¿Qué es exactamente una API? Una API, o Interfaz de Programación de Aplicaciones, es esencialmente un conjunto de reglas que permite que un software se comunique con otro.​



Para entenderlo mejor, podemos compararlo con una interfaz de usuario, pero a nivel de programación. Es la forma en que dos aplicaciones pueden hablar el mismo "idioma" y compartir datos de manera estructurada.​

</div>
</div>

---
layout: center
---

<div grid="~ cols-2">

<div class="self-center justify-self-center">

## Importancia

</div>

<div>

<div class="border-5 rounded-3xl border-dashed pr-4 pb-4">
<mdi-connection class="text-5xl text-orange-400" />

<div class="mt-2 ml-10">
La comunicación efectiva entre sistemas es crucial en el desarrollo de software. Aquí es donde entran en juego las APIs.​
</div>

</div>

<div class="border-5 rounded-3xl border-dashed pr-4 pb-4 mt-4">

<mdi-head-question class="text-5xl text-orange-400" />

<div class="mt-2 ml-10">
Imaginen las APIs como mensajeros que permiten a las aplicaciones hablar entre sí. Sin ellas, la integración y la colaboración entre sistemas serían extremadamente desafiantes.​
</div>

</div>
</div>
</div>

---
transition: slide-up
level: 2
---

## Características Clave de las API


<div grid="~ cols-4" class="min-h-full  content-center">
  <div class="h-24 w-1/2 rounded-1/2 bg-gray-500 m-2 grid items-center justify-self-center">
    <div class="justify-self-center">
      <mdi-key-variant class="text-5xl text-orange-400" />
    </div>
  </div>
  <div class="h-24 w-1/2 rounded-1/2 bg-gray-500 m-2 grid items-center justify-self-center">
    <div class="justify-self-center">
      <mdi-abacus class="text-5xl text-orange-400" />
    </div>
  </div>
  <div class="h-24 w-1/2 rounded-1/2 bg-gray-500 m-2 grid items-center justify-self-center">
    <div class="justify-self-center">
      <mdi-recycle class="text-5xl text-orange-400" />
    </div>
  </div>
  <div class="h-24 w-1/2 rounded-1/2 bg-gray-500 m-2 grid items-center justify-self-center">
    <div class="justify-self-center h-1/2">
      <mdi-tangram class="text-5xl text-orange-400" />
    </div>
  </div>
  <div class="p-6 text-center">
  Vamos a destacar algunas características esenciales.
  </div>
  <div class="p-6 text-center">
  La modularidad, actualizar componentes individualmente. 
  </div>
  <div class="p-6 text-center">
  La reutilización de código ​
  </div>
  <div class="p-6 text-center">
  La abstracción de complejidad son otras ventajas fundamentales.​
  </div>
</div>

---
transition: slide-up
level: 2
---

## Ejemplos de uso común​

<div grid="~ cols-6" class="content-center rounded-2 m-6 p-6 bg-gray-400 bg-opacity-60">
  <mdi-application-brackets class="text-5xl text-white-400" />
  <div class="col-span-5 text-5xl">
    Servicios Web
  </div>
</div>

<div grid="~ cols-6" class="content-center rounded-2 m-6 p-6 bg-gray-400 bg-opacity-60">
  <mdi-cellphone-cog class="text-5xl text-white-400" />
  <div class="col-span-5 text-5xl">
    Aplicaciones Móviles
  </div>
</div>

<div grid="~ cols-6" class="content-center rounded-2 m-6 p-6 bg-gray-400 bg-opacity-60">
  <mdi-chart-donut-variant class="text-5xl text-white-400" />  
  <div class="col-span-5 text-5xl">
    Integración de Plataformas​
  </div>
</div>

---
layout: image-right
image: imgs/searching-bug.jpg
---
<div class="text-center mb-8">

## Vulnerabilidades más comunes​

</div>

* Ssrf (Server Side Request Forgery)​

* Deserialización insegura ​

* Exceso de información ​

* Remote file read ​ ​

* IDOR

---
transition: slide-up
---

## Ssrf (Server Side Request Forgery)​


<div grid="~ cols-2" class="min-h-full">
  <mdi-panorama-variant-outline class="h-full text-9xl text-white-400  justify-self-center" />  
  <div class="grid place-content-end min-h-full">
    <p> Caso de Uso :​ </p>
    Supongamos que tienes una aplicación que 
    permite a los usuarios generar miniaturas 
    de imágenes proporcionando una URL de la img.
    La aplicación utiliza una función para 
    recuperar el contenido de la 
    URL proporcionada y generar la miniatura.​
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-4" class="min-h-full place-content-center">
  <mdi-shield-bug class="h-full text-8xl text-white-400 animate-ping" />  
  <div class="col-span-3 min-h-full justify-self-center">

```python

from flask import Flask request
import request

app = flask(__name__)

@app.route('/generate_thumbnail', methods=['POST'])
def generate_thumbnail():
  url = request.form.get('url')
  requests.get(url)
  return response.text

if __main__ == '__main__':
  app.run(debug=True)

```    
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-2" class="min-h-full">
  <mdi-redhat class="h-full text-9xl text-white-400  justify-self-center" />  
  <div class="grid min-h-full">
    <p> Ataque​ </p>
    Un atacante podría proporcionar una URL maliciosa como http://localhost/admin en lugar de una URL de imagen válida. Si la aplicación no valida adecuadamente la URL y permite solicitudes a recursos internos, el atacante podría obtener información confidencial o realizar acciones no autorizadas en el servidor.​
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-4" class="min-h-full place-content-center">
  <mdi-check-decagram class="h-full text-8xl text-white-400 justify-self-center" />  
  <div class="col-span-3 min-h-full justify-self-center">

```python

from flask import Flask request, jsonnify
import request
from urllib.parse import urlparse

app = flask(__name__)

@app.route('/generate_thumbnail', methods=['POST'])
def generate_thumbnail():
  url = request.form.get('url')

  parsed_url = urlparse(url)

  if not parsed_url.nectloc in ALLOWED_DOMAINS:
    return jsonnify({
      'error': 'Acceso no autorizado a la url'
    })
  requests.get(url)
  return response.text

if __main__ == '__main__':
  app.run(debug=True)

```    
  </div>
</div>








---
transition: slide-up
---

## Deserialización Insegura​


<div grid="~ cols-2" class="min-h-full">
  <mdi-cogs class="h-full text-9xl text-white-400  justify-self-center" />  
  <div class="grid place-content-end min-h-full">
    <p> Caso de Uso :​ </p>
    Imagina una aplicación que almacena y carga configuraciones 
    de usuarios en formato JSON. Al cargar estas configuraciones, 
    la aplicación utiliza la deserialización para convertir 
    los datos de JSON a objetos Python.​
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-4" class="min-h-full place-content-center">
  <mdi-shield-bug class="h-full text-8xl text-white-400 animate-ping" />  
  <div class="col-span-3 min-h-full justify-self-center">

```python

from flask import Flask request

app = flask(__name__)

@app.route('/load_user_config', methods=['POST'])
def load_user_config():
  user_config = request.form.get('config')
  config_obj = json.loads(user_config)
  return "Configuracion cargada."

if __main__ == '__main__':
  app.run(debug=True)

```    
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-2" class="min-h-full">
  <mdi-redhat class="h-full text-9xl text-white-400  justify-self-center" />  
  <div class="grid min-h-full">
    <p> Ataque​ </p>
    Un atacante podría manipular los datos de configuración JSON 
    y proporcionar un payload malicioso que, al deserializarse, 
    ejecute código no autorizado en el servidor.​
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-4" class="min-h-full place-content-center">
  <mdi-check-decagram class="h-full text-8xl text-white-400 justify-self-center" />  
  <div class="col-span-3 min-h-full justify-self-center">

```python

from flask import Flask request, jsonnify
import request
from urllib.parse import urlparse

app = flask(__name__)


@app.route('/load_user_config', methods=['POST'])
def load_user_config():
  user_config = request.form.get('config')
  try:
    config_obj = json.loads(user_config)
    return "Configuracion cargada."
  except json.JSONDecodeError:
    return jsonify({
      'error': 'Error al decodificar la configuracion JSON'
    })

if __main__ == '__main__':
  app.run(debug=True)

```    
  </div>
</div>





---
transition: slide-up
---

## Exceso de Información​


<div grid="~ cols-2" class="min-h-full">
  <mdi-folder-information class="h-full text-9xl text-white-400  justify-self-center" />  
  <div class="grid place-content-end min-h-full">
    <p> Caso de Uso :​ </p>
    Supongamos que tienes una API que devuelve información 
    sobre usuarios en función de su identificador. 
    La API proporciona detalles completos del usuario, 
    incluida información sensible.​
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-4" class="min-h-full place-content-center">
  <mdi-shield-bug class="h-full text-8xl text-white-400 animate-ping" />  
  <div class="col-span-3 min-h-full justify-self-center">

```python

from flask import Flask request

app = flask(__name__)

user_info_dict = {
  'user1': {
    'username': 'admin system',
    'email': 'admin@examples.com',
    'role': 'admin'
  },
  'user1': {
    'username': 'john_doe',
    'email': 'john@examples.com',
    'role': 'user'
  }
}

@app.route('/user_info', methods=['GET'])
def user_info():
  user_id = request.form.get('user_id')
  return user_info_dict.get(user_id, {})

if __main__ == '__main__':
  app.run(debug=True)

```    
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-2" class="min-h-full">
  <mdi-redhat class="h-full text-9xl text-white-400  justify-self-center" />  
  <div class="grid min-h-full">
    <p> Ataque​ </p>
    Un atacante podría solicitar información sobre un 
    usuario proporcionando un identificador y manipular 
    la solicitud para obtener más información de la necesaria, 
    posiblemente revelando datos sensibles.​
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-4" class="min-h-full place-content-center">
  <mdi-check-decagram class="h-full text-8xl text-white-400 justify-self-center" />  
  <div class="col-span-3 min-h-full justify-self-center">

```python
from flask import Flask request
app = flask(__name__)

user_info_dict = {
  'user1': {
    'username': 'admin system',
    'email': 'admin@examples.com',
    'role': 'admin'
  },
  'user1': {
    'username': 'john_doe',
    'email': 'john@examples.com',
    'role': 'user'
  }
}
@app.route('/user_info', methods=['GET'])
def user_info():
  user_id = request.form.get('user_id')
  if not user_id in user_info_dict:
    return jsonify({
      'error': 'usuario no encontrado'
    }), 404
  return user_info_dict.get(user_id, {}).get('username'. '')

if __main__ == '__main__':
  app.run(debug=True)

```    
  </div>
</div>






---
transition: slide-up
---

## Lectura Remota de Archivos​


<div grid="~ cols-2" class="min-h-full">
  <mdi-file-arrow-up-down-outline class="h-full text-9xl text-white-400  justify-self-center" />  
  <div class="grid place-content-end min-h-full">
    <p> Caso de Uso :​ </p>
    Imagina una aplicación que permite a 
    los usuarios proporcionar la ruta de un archivo y 
    devuelve su contenido.​

  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-4" class="min-h-full place-content-center">
  <mdi-shield-bug class="h-full text-8xl text-white-400 animate-ping" />  
  <div class="col-span-3 min-h-full justify-self-center">

```python

from flask import Flask request, jsonify
app = flask(__name__)

@app.route('/read_file', methods=['POST'])
def read_file():
  file_path = request.form.get('file_path')
  with open(file_path, 'r') as file:
    content = file.read()
  return content

if __main__ == '__main__':
  app.run(debug=True)

```    
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-2" class="min-h-full">
  <mdi-redhat class="h-full text-9xl text-white-400  justify-self-center" />  
  <div class="grid min-h-full">
    <p> Ataque​ </p>
    Un atacante podría proporcionar una ruta maliciosa 
    que apunte a archivos sensibles del sistema, 
    revelando información confidencial.​
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-4" class="min-h-full place-content-center">
  <mdi-check-decagram class="h-full text-8xl text-white-400 justify-self-center" />  
  <div class="col-span-3 min-h-full justify-self-center">

```python
from flask import Flask request
app = flask(__name__)

ALLOWED_PATH = '/some/allowed/path'

@app.route('/read_file', methods=['POST'])
def read_file():
  file_path = request.form.get('file_path')
  if not file_path in ALLOWED_PATH:
    return jsonify({
      'error': 'Archivo no autorizado'
    }), 404
  with open(file_path, 'r') as file:
    content = file.read()
  return content

if __main__ == '__main__':
  app.run(debug=True)

```    
  </div>
</div>



---
transition: slide-up
---

## IDOR (Insecure Direct Object References)​


<div grid="~ cols-2" class="min-h-full">
  <mdi-file-arrow-up-down-outline class="h-full text-9xl text-white-400  justify-self-center" />  
  <div class="grid place-content-end min-h-full">
    <p> Caso de Uso :​ </p>
    Imagina una aplicación de almacenamiento en la nube 
    que permite a los usuarios acceder a sus archivos 
    proporcionando un identificador de archivo.​
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-4" class="min-h-full place-content-center">
  <mdi-shield-bug class="h-full text-8xl text-white-400 animate-ping" />  
  <div class="col-span-3 min-h-full justify-self-center">

```python

from flask import Flask request, jsonify
app = flask(__name__)

user_files = {
  'user1': ['/files/user1/file1.txt', '/files/user1/file2.txt'']
  'user2': ['/files/user2/file1.txt', '/files/user2/file2.txt'']
}

@app.route('/get_user_file', methods=['GET'])
def get_user_file():
  user_id = request.args.get('user_id')
  file_index = int(request.args.get('file_index'))
  return user_files[user_id][file_index]

if __main__ == '__main__':
  app.run(debug=True)

```    
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-2" class="min-h-full">
  <mdi-redhat class="h-full text-9xl text-white-400  justify-self-center" />  
  <div class="grid min-h-full">
    <p> Ataque​ </p>
    Un atacante podría manipular el identificador 
    de archivo para acceder a recursos no autorizados, 
    como los archivos de otros usuarios.​
  </div>
</div>

---
transition: slide-up
---

<div grid="~ cols-4" class="min-h-full place-content-center">
  <mdi-check-decagram class="h-full text-8xl text-white-400 justify-self-center" />  
  <div class="col-span-3 min-h-full justify-self-center">

```python
from flask import Flask request
app = flask(__name__)

user_files = {
  'user1': ['/files/user1/file1.txt', '/files/user1/file2.txt']
  'user2': ['/files/user2/file1.txt', '/files/user2/file2.txt']
}

@app.route('/get_user_file', methods=['GET'])
def get_user_file():
  user_id = request.args.get('user_id')
  file_index = int(request.args.get('file_index'))
  if not user_id in user_files or not 0 < file_index <= len(user_files[user_id]):
    return jsonify({
      'error': 'Archivo no autorizado'
    }), 404
  return user_files[user_id][file_index]

if __main__ == '__main__':
  app.run(debug=True)

```    
  </div>
</div>

from flask import Flask, request, jsonify
from PIL import Image
from io import BytesIO
import requests

app = Flask(__name__)

@app.route('/generate_thumbnail', methods=['POST'])
def generate_thumbnail():
    url = request.form.get('url')
    
    try:
        response = requests.get(url)
        response.raise_for_status()  # Lanzar una excepción si la solicitud no es exitosa

        img = Image.open(BytesIO(response.content))
        img.thumbnail((100, 100))

        # Crear un objeto BytesIO para almacenar la miniatura
        thumbnail_buffer = BytesIO()
        img.save(thumbnail_buffer, format='PNG')  # Guardar la miniatura en formato PNG

        # Devolver la miniatura como respuesta
        return thumbnail_buffer.getvalue(), 200, {'Content-Type': 'image/png'}

    except requests.exceptions.RequestException as e:
        return jsonify({'error': f'Error al procesar la solicitud: {str(e)}'}), 400

    except Exception as e:
        return jsonify({'error': f'Error desconocido: {str(e)}'}), 500


@app.route('/generate_thumbnail_solved', methods=['POST'])
def generate_thumbnail():
    url = request.form.get('url')

    if not is_valid_url(url):
        return jsonify({'error': 'URL no permitida'}), 403
    
    try:
        response = requests.get(url)
        response.raise_for_status()  # Lanzar una excepción si la solicitud no es exitosa

        img = Image.open(BytesIO(response.content))
        img.thumbnail((100, 100))

        # Crear un objeto BytesIO para almacenar la miniatura
        thumbnail_buffer = BytesIO()
        img.save(thumbnail_buffer, format='PNG')  # Guardar la miniatura en formato PNG

        # Devolver la miniatura como respuesta
        return thumbnail_buffer.getvalue(), 200, {'Content-Type': 'image/png'}

    except requests.exceptions.RequestException as e:
        return jsonify({'error': f'Error al procesar la solicitud: {str(e)}'}), 400

    except Exception as e:
        return jsonify({'error': f'Error desconocido: {str(e)}'}), 500


if __name__ == '__main__':
    app.run(debug=True)

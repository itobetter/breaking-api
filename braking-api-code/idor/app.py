from flask import Flask, request, jsonify

app = Flask(__name__)

# Datos de usuario (¡Solo para propósitos educativos!)
user_files = {
    'user1': ['/etc/hosts', '/etc/hosts'],
    'user2': ['/etc/hosts', '/etc/hosts']
}

@app.route('/get_user_file', methods=['GET'])
def get_user_file():
    user_id = request.args.get('user_id')
    file_index = int(request.args.get('file_index'))
    return user_files[user_id][file_index]

@app.route('/get_user_file_solved', methods=['GET'])
def get_user_file_solved():
    user_id = request.args.get('user_id')
    file_index = int(request.args.get('file_index'))

    # Validar que el usuario tenga acceso al archivo solicitado
    if user_id in user_files and 0 <= file_index < len(user_files[user_id]):
        return user_files[user_id][file_index]
    else:
        return jsonify({'error': 'Acceso no autorizado al archivo'}), 403


if __name__ == '__main__':
    app.run(debug=True)

from flask import Flask, request, jsonify

app = Flask(__name__)

# Simulación de información de usuario (¡Solo para propósitos educativos!)
user_info_dict = {
    'user1': {'username': 'john_doe', 'email': 'john@example.com', 'role': 'admin'},
    'user2': {'username': 'jane_doe', 'email': 'jane@example.com', 'role': 'user'}
}

@app.route('/user_info', methods=['GET'])
def user_info():
    user_id = request.args.get('user_id')
    return user_info_dict[user_id]

@app.route('/user_info_solved', methods=['GET'])
def user_info_solved():
    user_id = request.args.get('user_id')
    if user_id in user_info_dict:
        return jsonify({'username': user_info_dict[user_id]['username']})
    else:
        return jsonify({'error': 'Usuario no encontrado'}), 404

if __name__ == '__main__':
    app.run(debug=True)

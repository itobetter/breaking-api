from flask import Flask, request
import json

app = Flask(__name__)

@app.route('/load_user_config', methods=['POST'])
def load_user_config():
    user_config = request.form.get('config')
    config_object = json.loads(user_config)
    return "Configuración cargada con éxito"


@app.route('/load_user_config_solved', methods=['POST'])
def load_user_config_solved():
    user_config = request.form.get('config')
    
    try:
        config_object = json.loads(user_config)
        # Validar y procesar la configuración del usuario de manera segura
        # ...
        return "Configuración cargada con éxito"
    except json.JSONDecodeError:
        return jsonify({'error': 'Error al decodificar la configuración JSON'}), 400

if __name__ == '__main__':
    app.run(debug=True)

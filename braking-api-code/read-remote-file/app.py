from flask import Flask, request

app = Flask(__name__)

@app.route('/read_file', methods=['POST'])
def read_file():
    file_path = request.form.get('file_path')
    with open(file_path, 'r') as file:
        content = file.read()
    return content

ALLOWED_PATHS = ['/home/']

@app.route('/read_file_solved', methods=['POST'])
def read_file_solved():
    file_path = request.form.get('file_path')
    if file_path in ALLOWED_PATHS:
        with open(file_path, 'r') as file:
            content = file.read()
        return content
    else:
        return jsonify({'error': 'Acceso no autorizado al archivo'}), 403



if __name__ == '__main__':
    app.run(debug=True)
